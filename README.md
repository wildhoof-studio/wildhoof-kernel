
## Wildhoof Kernel

Kernel Package with
* Dependency Injection
    * Interfaces
    * Singletons
    * Arguments
    * Autoresolve
* Simple Event Handler with priorities
* HTTP Messages
    * Requests and Responses
    * Streams
* HTTP Pipelines
* HTTP Server Interfaces for
    * Middleware
    * Handlers
* HTTP Response Factories
* Router with
    * Route parameters
    * Route middleware
    * Not Found Route

as it is used in Wildhoof projects.

### Requirements

* PHP 8.2

