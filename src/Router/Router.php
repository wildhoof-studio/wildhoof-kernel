<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

use Wildhoof\Kernel\Container\Container;

use Wildhoof\Kernel\Http\Message\Method;
use Wildhoof\Kernel\Http\Message\ServerRequest as Request;
use Wildhoof\Kernel\Http\Message\Response;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;

use Wildhoof\Kernel\Http\Pipeline\Queue;
use Wildhoof\Kernel\Http\Pipeline\Pipeline;

/**
 * Base request handler class with common dependencies.
 */
class Router implements Handler
{
    public function __construct(
        private readonly Container $container,
        private readonly Mapper $mapper,
        private readonly Matcher $matcher,
        private readonly Parser $parser
    ) {}

    /**
     * Add a placeholder token and its regular expression to the Matcher.
     */
    public function addToken(string $name, string $regex): void {
        $this->matcher->addToken($name, $regex);
    }

    /**
     * Add a GET Route to the Mapper.
     */
    public function get(string $pattern, string $action): Route
    {
        $route = new Route($pattern, Method::GET, $action);
        $this->mapper->addRoute($route);

        return $route;
    }

    /**
     * Add a POST Route to the Mapper.
     */
    public function post(string $pattern, string $action): Route
    {
        $route = new Route($pattern, Method::POST, $action);
        $this->mapper->addRoute($route);

        return $route;
    }

    /**
     * Add a PUT Route to the Mapper.
     */
    public function put(string $pattern, string $action): Route
    {
        $route = new Route($pattern, Method::PUT, $action);
        $this->mapper->addRoute($route);

        return $route;
    }

    /**
     * Add a DELETE Route to the Mapper.
     */
    public function delete(string $pattern, string $action): Route
    {
        $route = new Route($pattern, Method::DELETE, $action);
        $this->mapper->addRoute($route);

        return $route;
    }

    /**
     * Create a 404 Not Found fallback Route.
     */
    public function notFound(string $action): void {
        $this->mapper->setNotFound($action);
    }

    /**
     * Create a pipeline with route middleware and the handler
     */
    private function buildPipe(Handler $handler, array $middleware): Pipeline
    {
        $pipeline = new Pipeline(new Queue(), $handler);

        foreach ($middleware as $class) {
            $pipeline->pipe($this->container->get($class));
        }

        return $pipeline;
    }

    /**
     * Handles a request and produces a response.
     */
    public function handle(Request $request): Response
    {
        $handler = $this->mapper->getNotFound();
        $middleware = [];

        // Search for a matching route and return its action.
        foreach ($this->mapper->getRoutes() as $route)
        {
            if ($route->matchesRequest($this->matcher, $request))
            {
                $handler = $route->getHandler();
                $middleware = $route->getMiddleware();

                $request = $this->parser->parse($request, $route);
                break;
            }
        }

        $handler = $this->container->get($handler);
        $pipeline = $this->buildPipe($handler, $middleware);
        return $pipeline->handle($request);
    }
}
