<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;

use InvalidArgumentException;
use RuntimeException;

use function class_exists;
use function class_implements;
use function in_array;
use function sprintf;

/**
 * Registers and collects all Routes and therefore functions as the full
 * mapper of the application.
 */
class Mapper
{
    protected array $routes = [];
    protected string $notFound;

    /**
     * Adds a new Route to the end of the route array stack.
     */
    public function addRoute(Route $route): void {
        $this->routes[] = $route;
    }

    /**
     * Returns an array of all registered Routes.
     */
    public function getRoutes(): array {
        return $this->routes;
    }

    /**
     * Set the 404 Not Found fallback route.
     */
    public function setNotFound(string $handler): void
    {
        if (!class_exists($handler)) {
            $message = sprintf('Request handler %s does not exist!', $handler);
            throw new InvalidArgumentException($message);
        }
        
        if (!in_array(Handler::class, class_implements($handler))) {
            $message = 'Handler must be an implementation of';
            $message .= ' RequestHandlerInterface!';
            throw new InvalidArgumentException($message);
        }
        
        $this->notFound = $handler;
    }

    /**
     * Return the 404 Not Found fallback route.
     */
    public function getNotFound(): string
    {
        if (!isset($this->notFound)) {
            $message = 'Not Found Request Handler is not set!';
            throw new RuntimeException($message);
        }
        
        return $this->notFound;
    }
}
