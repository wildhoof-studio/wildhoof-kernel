<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Router;

/**
 * Router configuration.
 */
class Config
{
    public function __construct(
        private readonly string $placeholder = '[a-z]+',
        private readonly string $delimiter = ':',
        private readonly string $default = '[0-9]+'
    ) {}

    /**
     * Retrieve the placeholder char range.
     */
    public function getPlaceholder(): string {
        return $this->placeholder;
    }

    /**
     * Retrieve the delimiter character.
     */
    public function getDelimiter(): string {
        return $this->delimiter;
    }

    /**
     * Retrieve the default character range.
     */
    public function getDefault(): string {
        return $this->default;
    }
}
