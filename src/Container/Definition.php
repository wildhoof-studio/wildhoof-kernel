<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Container;

use ReflectionClass;

/**
 * Represents the definition of a class and its dependencies. By use of the
 * injected Container, it instantiates the class and resolves its dependencies.
 */
class Definition
{
    private array $dependencies = [];

    public function __construct(
        private readonly string $class,
        private readonly Container $container
    ) {}

    /**
     * Add a dependency by providing the registered classes alias. If no alias
     * has been provided, use the full classname instead.
     */
    public function needs(string|Argument $dependency): Definition
    {
        $this->dependencies[] = $dependency;

        // Return the instance for method chaining
        return $this;
    }

    /**
     * Returns a new instance of the described class.
     */
    public function __invoke(): object
    {
        $arguments = [];

        // Get all dependencies from the injected container
        foreach ($this->dependencies as $dependency)
        {
            if ($dependency instanceof Argument) {
                $arguments[] = $dependency->getValue();
            } else {
                $arguments[] = $this->container->get($dependency);
            }
        }

        // Create and return a new instance of the class
        $reflection = new ReflectionClass($this->class);
        return $reflection->newInstanceArgs($arguments);
    }
}
