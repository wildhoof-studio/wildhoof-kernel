<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Pipeline;

use Wildhoof\Kernel\Http\Server\RequestHandlerInterface as Handler;
use Wildhoof\Kernel\Http\Server\MiddlewareInterface;

use Wildhoof\Kernel\Http\Message\ServerRequest as Request;
use Wildhoof\Kernel\Http\Message\Response;

/**
 * Middleware pipeline.
 */
class Pipeline implements MiddlewareInterface, Handler
{
    private Queue $queue;
    private Handler $handler;

    public function __construct(Queue $queue, Handler $handler = null)
    {
        $this->queue = $queue;
        $this->handler = $handler;
    }

    /**
     * Add middleware to the middleware queue.
     */
    public function pipe(MiddlewareInterface $middleware): Pipeline
    {
        $this->queue->addToQueue($middleware);

        // Return self for fluent interface
        return $this;
    }

    /**
     * Processes the http request or delegates it to the handler.
     */
    public function process(Request $request, Handler $handler): Response {
        return (new Next($this->queue, $handler))->handle($request);
    }

    /**
     * Handles a request and produces a response.
     */
    public function handle(Request $request): Response {
        return $this->process($request, $this->handler);
    }
}
