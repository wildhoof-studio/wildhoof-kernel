<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Factory;

use Wildhoof\Kernel\Http\Message\MimeType;
use Wildhoof\Kernel\Http\Message\Response;
use Wildhoof\Kernel\Http\Message\Stream;

/**
 * Factory class for producing template HTML Responses
 */
final class HtmlResponseFactory
{
    /**
     * Creates a new instance of Response with HTML type and given status code.
     */
    public function create(int $code, string $html): Response
    {
        $response = new Response($code);

        // Add Content Type JSON to headers.
        $mimeType = MimeType::HTML->value;
        $response = $response->withHeader('Content-Type', $mimeType);

        // Add json as converted stream to body
        $stream = new Stream($html);
        return $response->withBody($stream);
    }
}
