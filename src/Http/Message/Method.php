<?php

declare(strict_types=1);

namespace Wildhoof\Kernel\Http\Message;

/**
 * Enum containing all supported HTTP Request methods.
 */
enum Method : string
{
    case GET = 'GET';
    case HEAD = 'HEAD';
    case POST = 'POST';
    case PUT = 'PUT';
    case DELETE = 'DELETE';
    case CONNECT = 'CONNECT';
    case OPTIONS = 'OPTIONS';
    case TRACE = 'TRACE';
    case PATCH = 'PATCH';
}
